# Installation
> `npm install --save @types/sanitize-html`

# Summary
This package contains type definitions for sanitize-html (https://github.com/punkave/sanitize-html).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/sanitize-html.

### Additional Details
 * Last updated: Sun, 18 Aug 2024 19:35:50 GMT
 * Dependencies: [htmlparser2](https://npmjs.com/package/htmlparser2)

# Credits
These definitions were written by [Rogier Schouten](https://github.com/rogierschouten), [Afshin Darian](https://github.com/afshin), [Rinze de Laat](https://github.com/biermeester), [Johan Davidsson](https://github.com/johandavidson), [Jianrong Yu](https://github.com/YuJianrong), [GP](https://github.com/paambaati), [Dariusz Syncerek](https://github.com/dsyncerek), [Piotr Błażejewicz](https://github.com/peterblazejewicz), [Pirasis Leelatanon](https://github.com/1pete), [Alex Rantos](https://github.com/alex-rantos), and [Dylan Armstrong](https://github.com/dylanarmstrong).
